import { Component, OnInit } from '@angular/core';
import { VendorService } from 'src/app/services/vendor.service';
import { InvoiceService } from 'src/app/services/invoice.service';

@Component({
  selector: 'app-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['./shell.component.styl']
})
export class ShellComponent implements OnInit {

  constructor(
    private vendorService: VendorService,
    private invoiceService: InvoiceService) { }

  ngOnInit() {
    this.vendorService.getVendor().subscribe(res => console.log(res));
    this.invoiceService.getInvoice().subscribe(res => console.log(res));
  }

}
