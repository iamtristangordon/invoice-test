import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class VendorService {

  constructor(
    private http: HttpClient) { }

  getVendor(): Observable<any> {
    return this.http.get('../../assets/data/vendor.json');
  }
}
