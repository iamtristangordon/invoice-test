export interface Memo {
    writtenBy: string;
    text: string; 
}