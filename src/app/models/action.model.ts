export interface Action {
    date: number;
    type: number;
    performedBy: string;
    description: string;
}