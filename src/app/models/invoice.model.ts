import { Memo } from './memo.model';
import { Action } from './action.model';
import { Lineitem } from './line-item.model';

export interface Vendor {
    invoiceNumber: string;
    invoiceType: string;
    restaurant: string;
    tax: number;
    subTotal: number;
    total: number;
    postingDate: number;
    invoiceDate: number;
    dueDate: number;
    memos: Memo[];
    actionHistory: Action[];
    lineItems: Lineitem[];
}