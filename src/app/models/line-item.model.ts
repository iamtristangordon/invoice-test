export interface Lineitem {
    name: string;
    type: string;
    quantity: number;
    unitPrice: number;
    total: number;
}