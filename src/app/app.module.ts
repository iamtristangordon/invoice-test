import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ShellComponent } from './containers/shell/shell.component';
import { InvoiceComponent } from './components/invoice/invoice.component';
import { InvoiceDataComponent } from './components/invoice-data/invoice-data.component';

@NgModule({
  declarations: [
    AppComponent,
    ShellComponent,
    InvoiceComponent,
    InvoiceDataComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
